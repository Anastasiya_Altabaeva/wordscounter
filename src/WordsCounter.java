import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Класс, который позволяет считать количество символов с пробелами,
 * количество символов без пробелов и количество слов.
 *
 * @author Алтабаева А.Р. 16ИТ18к
 */

public class WordsCounter {

    public static void main(String[] args) {
        ArrayList text = new ArrayList();
        try {
            FileInputStream inputStream = new FileInputStream("text.txt");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (!line.isEmpty()) {
                    text.add(line);
                }
            }
        } catch (IOException e) {
            System.out.println("Ошибка");
        }

        String[] myText = transfer(text);
        int symbolsWithOutSpase = countSymbolsWithoutSpase(myText);
        int symbolsWithSpase = countSymbolsWithSpase(myText);
        int word = counterWords(myText);
        System.out.printf("Количество символов c пробелами %d , количество символов без пробелов %d , количество слов %d   ", symbolsWithSpase, symbolsWithOutSpase, word);
    }

    /**
     * Метод, возвращающий количество символов без пробелов.
     *
     * @param myText - массив строк.
     * @return количество символов без пробелов.
     *
     */
    private static int countSymbolsWithoutSpase(String[] myText) {
        int counter = 0;
        for (String string : myText) {
            for (int i = 0; i < string.length(); i++) {
                if (string.charAt(i) != ' ') {
                    counter++;
                }
            }
        }
        return counter;
    }

    /**
     * Метод, возвращающий количество символов с пробелами.
     *
     * @param myText - массив строк.
     * @return количество символов с пробелами.
     *
     */
    private static int countSymbolsWithSpase(String[] myText) {
        int counter = 0;
        for (String string : myText) {
            counter += string.length();
        }
        return counter;
    }

    /**
     * Метод, возвращающий количество слов.
     *
     * @param myText - массив строк.
     * @return количество слов.
     *
     */
    private static int counterWords(String[] myText) {
        int count = 0;
        for (String string : myText) {
            string = string.replaceAll("[^-aA-zZаА-яЯ0-9 ]", " ");
            string = string.replaceAll("—", "");
            string = string.replaceAll(" {2,}", " ");
            for (String retval : string.split(" ")) {
                count++;
            }
        }
        return count;
    }

    /**
     * Метод, который переводит ArrayList в массив.
     *
     * @param text - ArrayList с исходным текстом.
     * @return - массив строк.
     */
    private static String[] transfer(ArrayList text) {
        String[] myText = {};
        int y = text.size();
        myText = (String[]) text.toArray(new String[y]);
        return myText;
    }
}

